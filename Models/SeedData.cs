﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using IssueTracker.Data;
using System;
using System.Linq;

namespace IssueTracker.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new IssueTrackerContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<IssueTrackerContext>>()))
            {
                // Look for any movies.
                if (!context.Product.Any())
                {
                    context.Product.AddRange(
                        new Product
                        {
                            Title = "Product1",
                            CreatedDate = DateTime.Parse("1989-2-12")
                        },

                        new Product
                        {
                            Title = "Product2",
                            CreatedDate = DateTime.Parse("1989-2-13")
                        },

                        new Product
                        {
                            Title = "Product3",
                            CreatedDate = DateTime.Parse("1972-2-12")
                        }
                    );
                    context.SaveChanges();
                }


                // Look for any movies.
                if (!context.Issue.Any())
                {
                    context.Issue.AddRange(
                        new Issue
                        {
                            Title = "Typo on label",
                            Description = "Typo on label on the description page.",
                            Author = "Adrian",
                            Product_Id = 1,
                            CreatedDate = DateTime.Parse("1989-2-12"),
                            UpdatedDate = DateTime.Parse("2000-1-02")
                        },

                        new Issue
                        {
                            Title = "Typo on header",
                            Description = "Typo on header on the description page.",
                            Author = "Adrian",
                            Product_Id = 2,
                            CreatedDate = DateTime.Parse("1999-2-12"),
                            UpdatedDate = DateTime.Parse("2001-1-02")
                        },

                        new Issue
                        {
                            Title = "Typo on title",
                            Description = "Typo on title of the description page.",
                            Author = "Joe",
                            Product_Id = 2,
                            CreatedDate = DateTime.Parse("1979-2-12"),
                            UpdatedDate = DateTime.Parse("2010-1-02")
                        }

                    );
                    context.SaveChanges();
                }



            }
        }
    }
}