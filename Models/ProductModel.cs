﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IssueTracker.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        //[DataType(DataType.Date)]
        public DateTime CreatedDate { get; set; }
    }
}