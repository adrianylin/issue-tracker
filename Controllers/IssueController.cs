﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IssueTracker.Data;
using IssueTracker.Models;

namespace IssueTracker.Controllers
{
    public class IssueController : Controller
    {
        private readonly IssueTrackerContext _context;

        public IssueController(IssueTrackerContext context)
        {
            _context = context;
        }

        // GET: Issue
        public async Task<IActionResult> Index(string product, string status, string titleSearch)
        {
            var products = from p in _context.Issue
                           select p;

            if (!String.IsNullOrEmpty(titleSearch))
            {
                products = products.Where(s => s.Title.Contains(titleSearch));
                products = products.OrderByDescending(s => s.UpdatedDate);
            }

            else if (!String.IsNullOrEmpty(product) && !String.IsNullOrEmpty(status))
            {
                products = products.Where(s => s.Product_Id.ToString().Equals(product) && s.Status.ToString().Equals(status));
                products = products.OrderByDescending(s => s.UpdatedDate);
            }

            return View(await products.ToListAsync());
        }

        // GET: Issue/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var issue = await _context.Issue
                .FirstOrDefaultAsync(m => m.Id == id);
            if (issue == null)
            {
                return NotFound();
            }

            return View(issue);
        }

        // GET: Issue/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Issue/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Description,Author,Status,Priority")] Issue issue, string product)
        {
            if (ModelState.IsValid)
            {
                issue.Product_Id = int.Parse(product); //TODO: Add exception handling
                issue.CreatedDate = DateTime.Now;
                issue.UpdatedDate = DateTime.Now;
                _context.Add(issue);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), "Issue", new { product = issue.Product_Id, status=0 });
            }
            return View(issue);
        }

        // GET: Issue/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var issue = await _context.Issue.FindAsync(id);
            if (issue == null)
            {
                return NotFound();
            }
            return View(issue);
        }

        // POST: Issue/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Description,Author,Status,Priority")] Issue issue, string product)
        {
            if (id != issue.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    issue.Product_Id = int.Parse(product); //TODO: Add exception handling
                    issue.CreatedDate = DateTime.Now;
                    issue.UpdatedDate = DateTime.Now;
                    _context.Update(issue);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!IssueExists(issue.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), "Issue", new { product = issue.Product_Id, status=0 });
            }
            return View(issue);
        }

        // GET: Issue/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var issue = await _context.Issue
                .FirstOrDefaultAsync(m => m.Id == id);
            if (issue == null)
            {
                return NotFound();
            }

            return View(issue);
        }

        // POST: Issue/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var issue = await _context.Issue.FindAsync(id);
            _context.Issue.Remove(issue);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), "Issue", new { product = issue.Product_Id, status=0 });
        }

        private bool IssueExists(int id)
        {
            return _context.Issue.Any(e => e.Id == id);
        }
    }
}
